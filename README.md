# Documentation:


![alt text](https://gitlab.com/ITOpsTube-Webinars/packer-redhatami-demo/raw/a131c631a06f250a535158216e73b199acb1f513/Packer.PNG)


- **Tools used**
   - Packer
   - Shell script
   - Ansible



- **Packer Execution flow**
   - [x] Once the packer build command was executed, packer will create a temporary EC2 instance
   - [x] On temporary EC2 instance, provision.sh will be automatically executed by Packer provisioners
   - [x] As provision shell script we are cooking some required packages like (python,wget,python2-pip,ansible and awscli)
       - Note: ansible package will be cooked for temporary purpose
   - [x] Once the required packages are installed, we started running ansible playbook e.g. (ansible-playbook redhatami-oldcis/main-site.yml)
   - [x] Using ansible playbook, we are performing all the CIS CAT recommendations to the OS
   - [x] Finally by executing the CIS CAT script and generating the CIS report and uploading to S3 bucket 
   - [x] Once all the playbook steps are completed, Packer will automatically build the Golden AMI and terminate the temporary EC2 instance
   - [x] We can review the CIS report, if everything is good, we can finally release the Golden AMI to ALL



**Steps to create REDHAT AMI**

Prerequisite:

1 - Create a ec2 instance and give ec2full access role to it

2 - Dowload packer zip from https://www.packer.io/downloads.html
```
sudo yum install wget git -y
wget https://releases.hashicorp.com/packer/1.4.2/packer_1.4.2_linux_amd64.zip
unzip packer_1.4.2_linux_amd64.zip
sudo mv packer /bin/packer
packer --version
``` 

After cloning the git reop please execute below commands from redhatami folder


1 - Run below packer command to build  redhat custom ami on us-west-2

```
cd redhatami-oldcis
packer build packer-golden-image-ami-1.json

```




