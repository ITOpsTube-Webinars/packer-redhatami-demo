#!/usr/bin/bash

set -e
sudo yum install -y epel-release
sudo yum install -y ansible python2-pip
sudo pip install awscli
echo "Extracting centosami-oldcis.tar"
cd /home/centos
pwd
ls -ltr
tar -xf centosami-oldcis.tar
echo "Running build"
ansible-playbook centosami-oldcis/main-site.yml
sudo yum remove -y ansible epel-release
rm -rf centosami-oldcis ~/centosami-oldcis.tar /root/CIS-CAT_Results /opt/cis-cat-full.zip /opt/cis-cat-full
