#!/usr/bin/bash

set -e
sudo yum install -y python36
sudo pip3 install ansible awscli
echo "Extracting centosami-cis.tar"
cd /home/centos
pwd
ls -ltr
tar -xf centosami-cis.tar
echo "Running build"
ansible-playbook centosami-cis/main-site.yml
sudo pip3 uninstall ansible -y
rm -rf ~/centosami-cis ~/centosami-cis.tar
