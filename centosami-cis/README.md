Steps to create REDHAT AMI

After cloning the git reop please execute below commands from redhatami folder

1 - Create ssh keys for your user and place it hear

```
ssh-keygen -f emma_id_rsa
```

2 - Run below packer command to build  redhat custom ami on us-west-2

```
packer build packer-golden-redhat-ami.json

```
