#!/usr/bin/bash

set -e
sudo yum install -y python wget unzip
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install -y epel-release-latest-7.noarch.rpm
sudo yum install -y python2-pip
sudo pip install awscli

sudo cat /etc/hosts
sudo cp /etc/hosts /opt/hosts
sudo sed -i 's/127.0.0.1   localhost/127.0.0.1 localhost salt/g' /etc/hosts 

sudo cat /etc/hosts

sudo echo "[saltstack-repo]" > ~/salt.repo
sudo echo "name=SaltStack repo for Red Hat Enterprise Linux" >> ~/salt.repo
sudo echo "baseurl=https://repo.saltstack.com/yum/redhat/7.6/x86_64/latest" >> ~/salt.repo
sudo echo "enabled=1" >> ~/salt.repo
sudo echo "gpgcheck=1" >> ~/salt.repo
sudo echo "gpgkey=https://repo.saltstack.com/yum/redhat/7.6/x86_64/latest/SALTSTACK-GPG-KEY.pub" >> ~/salt.repo
sudo mv ~/salt.repo /etc/yum.repos.d/salt.repo
sudo cat /etc/yum.repos.d/salt.repo
sudo yum install salt-master -y
sudo yum install salt-minion -y
sudo yum install salt-ssh -y

sudo systemctl start salt-master
sudo systemctl start salt-minion
sudo systemctl status salt-master -l
sudo systemctl status salt-minion -l

sudo salt-key -L
sleep 20
sudo salt-key -A -y
sleep 10
sudo salt-key -L

#sudo systemctl restart salt-minion
#sleep 20
#sudo salt-key -L

echo "Extracting packer-salt-redhatami.tar"
cd /home/ec2-user
pwd
ls -ltr
tar -xf packer-salt-redhatami.tar
sudo cp -rvf packer-salt-redhatami/salt /srv/.
echo "Running build"
sudo salt '*' state.sls ami
sudo rm -rf ~/epel-release-latest-7.noarch.rpm ~/packer-salt-redhatami ~/packer-salt-redhatami.tar /root/CIS-CAT_Results /opt/cis-cat-full.zip /opt/cis-cat-full
sudo yum remove salt-master salt-minion salt-ssh epel-release -y
sudo mv /opt/hosts /etc/hosts
