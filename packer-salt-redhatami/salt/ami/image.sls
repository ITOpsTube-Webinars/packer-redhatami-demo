---
base.packages:
  pkg.installed:
    - pkgs:
      - java-1.8.0-openjdk
      - java-1.8.0-openjdk-devel
      - unzip
      - aide

run_get_cis-cat-full_zip:
  cmd.run:
    #- name: aws s3 cp s3://ptc-cis-cat/ciscat-full-bundle-2019-07-26-v3.0.60.zip /opt/ciscat-full-bundle-2019-07-26-v3.0.60.zip
    - name: aws s3 cp s3://cis-cat-check/cis-cat-full.zip /opt/cis-cat-full.zip

extract_myapp:
  archive.extracted:
    - name: /opt
    #- source: /opt/ciscat-full-bundle-2019-07-26-v3.0.60.zip
    - source: /opt/cis-cat-full.zip
    - user: root
    - group: root
    - if_missing: /opt/cis-cat-full

/opt/cis-cat-full/CIS-CAT.sh:
  file.managed:
    - user: root
    - group: root
    - mode: 755

/etc/sysctl.conf:
  file.append:
    - text:
      - 'kernel.randomize_va_space = 2'
      - 'net.ipv4.ip_forward = 0'
      - 'net.ipv4.conf.default.send_redirects = 0'
      - 'net.ipv4.conf.all.send_redirects=0'
      - 'fs.suid_dumpable = 0'
      - 'net.ipv4.conf.default.accept_source_route = 0'
      - 'net.ipv4.conf.all.accept_source_route = 0'
      - 'net.ipv4.conf.all.accept_redirects = 0'
      - 'net.ipv4.conf.default.accept_redirects = 0'
      - 'net.ipv4.conf.all.secure_redirects = 0'
      - 'net.ipv4.conf.default.secure_redirects = 0'
      - 'net.ipv4.conf.all.log_martians = 1'
      - 'net.ipv4.conf.default.log_martians = 1'
      - 'net.ipv4.icmp_echo_ignore_broadcasts = 1'
      - 'net.ipv4.icmp_ignore_bogus_error_responses = 1'
      - 'net.ipv4.conf.all.rp_filter = 1'
      - 'net.ipv4.conf.default.rp_filter = 1'
      - 'net.ipv4.tcp_syncookies = 1'
      - 'net.ipv6.conf.all.accept_ra = 0'
      - 'net.ipv6.conf.default.accept_ra = 0' 

/etc/modprobe.d/CIS.conf:
  file.append:
    - text: 
      - 'install cramfs /bin/true'
      - 'install freevxfs /bin/true'
      - 'install jffs2 /bin/true'
      - 'install hfs /bin/true'
      - 'install hfsplus /bin/true'
      - 'install squashfs /bin/true'
      - 'install udf /bin/true'

run_mount_shm:
  cmd.run:
    - name: mount -o remount,noexec /dev/shm

/usr/sbin/aide --check > /dev/null:
  cron.present:
    - user: root
    - minute: 0
    - hour: 5

/boot/grub2/grub.cfg:
  file.managed:
    - user: root
    - group: root
    - mode: 600

/boot/grub2/user.cfg:
  file.append:
    - text: 
      - 'GRUB2_PASSWORD=fghrdegh'

/etc/security/limits.conf:
  file.append:
    - text: '* hard core 0'

/etc/security/limits.d/20-nproc.conf:
  file.append:
    - text: '* hard core 0'

nfs:
  service.dead:
    - enable: False

rpcbind:
  service.dead:
    - enable: False

run_fs.suid_dumpable:
  cmd.run:
    - name: sysctl -w fs.suid_dumpable=0

run_routed_packets_are_not_accepted_net.ipv4.conf.all.accept_source_route:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.all.accept_source_route=0

run_routed_packets_are_not_accepted_net.ipv4.conf.default.accept_source_route:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.default.accept_source_route=0

run_routed_packets_are_not_accepted_net.ipv4.conf.all.accept_redirects:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.all.accept_redirects=0

run_routed_packets_are_not_accepted_net.ipv4.conf.default.accept_redirects:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.default.accept_redirects=0 

run_routed_packets_are_not_accepted_net.ipv4.conf.all.secure_redirects:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.all.secure_redirects=0

run_routed_packets_are_not_accepted_net.ipv4.conf.default.secure_redirects:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.default.secure_redirects=0 

run_routed_packets_are_not_accepted_net.ipv4.conf.all.log_martians:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.all.log_martians=1 
     
run_routed_packets_are_not_accepted_net.ipv4.conf.default.log_martians:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.default.log_martians=1 

run_routed_packets_are_not_accepted_net.ipv4.icmp_echo_ignore_broadcasts:
  cmd.run:
    - name: sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1 

run_routed_packets_are_not_accepted_net.ipv4.icmp_ignore_bogus_error_responses:
  cmd.run:
    - name: sysctl -w net.ipv4.icmp_ignore_bogus_error_responses=1

run_routed_packets_are_not_accepted_net.ipv4.conf.all.rp_filter:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.all.rp_filter=1 

run_routed_packets_are_not_accepted_net.ipv4.conf.default.rp_filter:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.default.rp_filter=1 

run_routed_packets_are_not_accepted_net.ipv4.tcp_syncookies:
  cmd.run:
    - name: sysctl -w net.ipv4.tcp_syncookies=1 

run_routed_packets_are_not_accepted_kernel.randomize_va_space:
  cmd.run:
    - name: sysctl -w kernel.randomize_va_space=2

run_net.ipv4.conf.all.send_redirects:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.all.send_redirects=0

run_net.ipv4.conf.default.send_redirects:
  cmd.run:
    - name: sysctl -w net.ipv4.conf.default.send_redirects=0

run_net.ipv6.conf.all.accept_ra:
  cmd.run:
    - name: sysctl -w net.ipv6.conf.all.accept_ra=0

run_net.ipv6.conf.default.accept_ra:
  cmd.run:
    - name: sysctl -w net.ipv6.conf.default.accept_ra=0

run_net.ipv6.route.flush:
  cmd.run:
    - name: sysctl -w net.ipv6.route.flush=1

run_net.ipv4.route.flush:
  cmd.run:
    - name: sysctl -w net.ipv4.route.flush=1

/etc/rsyslog.conf:
  file.append:
    - text:
      - '$FileCreateMode 0640'
      - '*.* @@loghost.example.com'

run_Ensure_permissions_on_all_logfiles_are_configured:
  cmd.run:
    - name: find /var/log -type f -exec chmod g-wx,o-rwx {} +

/etc/crontab:
  file.managed:
    - user: root
    - group: root
    - mode: 600

/etc/cron.hourly:
  file.directory:
    - user: root
    - group: root
    - mode: 600

/etc/cron.daily:
  file.directory:
    - user: root
    - group: root
    - mode: 600

/etc/cron.weekly:
  file.directory:
    - user: root
    - group: root
    - mode: 600

/etc/cron.monthly:
  file.directory:
    - user: root
    - group: root
    - mode: 600

/etc/cron.d:
  file.directory:
    - user: root
    - group: root
    - mode: 700

/etc/cron.deny:
  file.absent:
    - name: /etc/cron.deny

/etc/at.deny:
  file.absent:
    - name: /etc/at.deny

/etc/cron.allow:
  file.touch:
    - name: /etc/cron.allow

/etc/at.allow:
  file.touch:
    - name: /etc/at.allow

/etc/ssh/sshd_config:
  file.append:
    - text:
      - 'Protocol 2'
      - 'LogLevel INFO'
      - 'X11Forwarding no'
      - 'MaxAuthTries 4'
      - 'IgnoreRhosts yes'
      - 'HostbasedAuthentication no'
      - 'PermitRootLogin no'
      - 'PermitEmptyPasswords no'
      - 'PermitUserEnvironment no'
      - 'MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com'
      - 'ClientAliveInterval 300'
      - 'ClientAliveCountMax 0'
      - 'LoginGraceTime 60'
      - 'Banner /etc/issue.net'

/etc/pam.d/password-auth:
  file.append:
    - text:
      - 'password requisite pam_pwquality.so try_first_pass retry=3'
      - 'password sufficient pam_unix.so remember=5'
      - 'auth required pam_faillock.so preauth audit silent deny=5 unlock_time=900'
      - 'auth [success=1 default=bad] pam_unix.so'
      - 'auth [default=die] pam_faillock.so authfail audit deny=5 unlock_time=900'
      - 'auth sufficient pam_faillock.so authsucc audit deny=5 unlock_time=900'

/etc/pam.d/system-auth:
  file.append:
    - text:
      - 'password requisite pam_pwquality.so try_first_pass retry=3'
      - 'password sufficient pam_unix.so remember=5'
      - 'auth required pam_faillock.so preauth audit silent deny=5 unlock_time=900'
      - 'auth [success=1 default=bad] pam_unix.so'
      - 'auth [default=die] pam_faillock.so authfail audit deny=5 unlock_time=900'
      - 'auth sufficient pam_faillock.so authsucc audit deny=5 unlock_time=900' 

/etc/pam.d/su:
  file.append:
    - text:
      - 'auth required pam_wheel.so use_uid'


/etc/security/pwquality.conf:
  file.append:
    - text:
      - 'minlen=14'
      - 'dcredit=-1'
      - 'ucredit=-1'
      - 'ocredit=-1'
      - 'lcredit=-1'

/etc/login.defs:
  file.append:
    - text:
      - 'PASS_MAX_DAYS 90'
      - 'PASS_MIN_DAYS 7'

run_default_password_inactivity_period_to_30_days:
  cmd.run:
    - name: useradd -D -f 30

/etc/bashrc:
  file.line:
    - match: 'umask 002'
    - content: 'umask 027'
    - mode: replace
     
/etc/profile:
  file.line:
    - match: 'umask 002'
    - content: 'umask 027'
    - mode: replace

rsyslog:
  service.dead:
    - enable: True

run_yum_install_tcp_wrappers:
  cmd.run:
    - name: yum install tcp_wrappers -y 

run_yum_remove_xorg:
  cmd.run:
    - name: yum remove xorg-x11* -y 

run_CIS-CAT.sh:
  cmd.run:
    - name: sh CIS-CAT.sh -b benchmarks/CIS_Red_Hat_Enterprise_Linux_7_Benchmark_v2.2.0-xccdf.xml -a 
    - cwd: /opt/cis-cat-full

run_CIS-CAT_upload:
  cmd.run:
    #- name: aws s3 sync /root/CIS-CAT_Results/ s3://ptc-cis-cat/Report/Redhat-cis/
    - name: aws s3 sync /root/CIS-CAT_Results/ s3://cis-cat-check/Report/Redhat-cis/

run_package_cleanup:
  cmd.run:
    - name: /bin/package-cleanup --oldkernels --count=1

run_yum_clean:
  cmd.run:
    - name: yum clean all

/var/log/dmesg.old:
  file.absent:
    - name: /var/log/dmesg.old

/var/log/anaconda:
  file.absent:
    - name: /var/log/anaconda

remove_tmp_dir:
   file.directory:
      - name: /tmp/           
      - clean: True

remove_var_tmp_dir:
   file.directory:
      - name: /var/tmp/                    
      - clean: True

remove_cis_result_dir:
   file.directory:
      - name: /root/CIS-CAT_Results
      - clean: True

/root/.bash_history:
  file.absent:
    - name: /root/.bash_history

