#!/usr/bin/bash

set -e
sudo yum install -y python36
sudo pip3 install ansible
echo "Extracting redhatami.tar"
cd /home/ec2-user
pwd
ls -ltr
tar -xf redhatami.tar
echo "Running build"
ansible-playbook redhatami/main-site.yml
sudo pip3 uninstall ansible -y
rm -rf ~/redhatami ~/redhatami.tar
