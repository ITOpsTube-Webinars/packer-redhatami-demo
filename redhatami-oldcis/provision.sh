#!/usr/bin/bash

set -e
sudo yum install -y python wget
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install -y epel-release-latest-7.noarch.rpm
sudo yum install -y python2-pip
sudo pip install ansible awscli
echo "Extracting redhatami-oldcis.tar"
cd /home/ec2-user
pwd
ls -ltr
tar -xf redhatami-oldcis.tar
echo "Running build"
ansible-playbook redhatami-oldcis/main-site.yml
sudo pip uninstall ansible -y
sudo rm -rf ~/redhatami-oldcis ~/redhatami-oldcis.tar ~/epel-release-latest-7.noarch.rpm /root/CIS-CAT_Results /opt/cis-cat-full.zip /opt/cis-cat-full
sudo yum remove epel-release -y
